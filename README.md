# Yahoo Weather Fetcher

A simple Python 3 script to fetch the weather details for a city and save the data to a file.

## Installation

- Clone the repository
- Create a config.ini for your environment
- Add the fetcher to crontab

```bash
# weather fetch
*/5 * * * * python3 /opt/proxy/yahoo-weather-fetcher/yahoo-weather-fetcher.py --config=/opt/proxy/yahoo-weather-fetcher/config.ini
```
