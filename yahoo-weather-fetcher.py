##################################################################
## yeahoo-weather-fetcher.py                                    ##
## a simple utility to fetch the weather for a location         ##
## ideally used as a cron job to periodically fetch the weather ##
## Developed Spring 2019 by Brady Bilderback                    ##
##################################################################

# you should really read the documentation before editing this
# https://developer.yahoo.com/weather/documentation.html#oauth-python

# standard imports
import time, uuid, sys, getopt, configparser, ast

# imports for url and http requests
import urllib, urllib.parse, urllib.request

# imports for hashing
import hmac, hashlib
from base64 import b64encode


if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # GENERAL
    weather_output = config['GENERAL']['weather_output']
    weather_location = config['GENERAL']['weather_location']
    weather_unit = config['GENERAL']['weather_unit']
    weather_format = config['GENERAL']['weather_format']

    # YAHOO_API_KEYS
    app_id = config['YAHOO_API_KEYS']['app_id']
    consumer_key = config['YAHOO_API_KEYS']['consumer_key']
    consumer_secret = config['YAHOO_API_KEYS']['consumer_secret']

    # yahoo weather url info
    method = 'GET'
    url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss'
    query = {'location': weather_location, 'u': weather_unit, 'format': weather_format}
    concat = '&'

    # yahoo dev oauth info
    oauth = {
        'oauth_consumer_key': consumer_key,
        'oauth_nonce': uuid.uuid4().hex,
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': str(int(time.time())),
        'oauth_version': '1.0'
    }

    # create the oauth signature and authorization
    merged_params = query.copy()
    merged_params.update(oauth)
    sorted_params = [k + '=' + urllib.parse.quote(merged_params[k], safe='') for k in sorted(merged_params.keys())]
    signature_base_str =  method + concat + urllib.parse.quote(url, safe='') + concat + urllib.parse.quote(concat.join(sorted_params), safe='')

    composite_key = urllib.parse.quote(consumer_secret, safe='') + concat
    oauth_signature = b64encode(hmac.new(composite_key.encode('utf-8'), signature_base_str.encode('utf-8'), hashlib.sha1).digest()).decode('utf-8')

    oauth['oauth_signature'] = oauth_signature
    auth_header = 'OAuth ' + ', '.join(['{}="{}"'.format(k,v) for k,v in iter(oauth.items())])

    # do the request for weather info
    url = url + '?' + urllib.parse.urlencode(query)
    request = urllib.request.Request(url)
    request.add_header('Authorization', auth_header)
    request.add_header('Yahoo-App-Id', app_id)
    response = urllib.request.urlopen(request).read()

    # write out the received data
    open(weather_output, 'wb').write(response)

    sys.exit(0)